import React from 'react';
import { render } from 'react-dom';
import SearchApp from './Components/SearchApp/SearchApp';

const appEl = document.getElementById('app');

render(<SearchApp />, appEl);