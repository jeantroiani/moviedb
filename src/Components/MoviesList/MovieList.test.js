import { expect } from 'chai';
import MoviesList from './MoviesList';
import React, { Component } from 'react';
import { shallow } from 'enzyme';

describe('MovieList', () => {
    it('it renders a list', () => {
        const movies = [
            {
                title: "Ratatouille"
            }
        ];
        const wrapper = shallow(<MoviesList movies={ movies }/>);
        expect(wrapper.html())
            .to
            .equal(`<div><h1>Search result:</h1><ul><li>Ratatouille</li></ul></div>`);
    });
});
