import React, { PropTypes } from 'react';

export default function MoviesList(props) {
    const {movies} = props;
    return (
        <div>
            <h1>Search result:</h1>
            <ul>
                {movies.map((movie, index) => {
                    return <li key={index}>{movie.title}</li>;
                })}
            </ul>
        </div>
    );
}