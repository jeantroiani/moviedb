import React, {PropTypes} from 'react';

export default function SearchForm(props) {
    let userText = null;
    const {name} = props;

    function handleSubmit(e) {
        e.preventDefault();
        const {search} = props;
        search(userText.value);
    }
    return (
        <form>
            <label htmlFor="input-search-movie">Search:</label>
            <input
                ref={(c) => {
                userText = c;
            }}
                id="input-search-movie"
                type="text"
                placeholder="Search for movie title"/>
            <input onClick={handleSubmit} type="submit" value="Search"/>
        </form>
    );
}