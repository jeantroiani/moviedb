import {expect} from 'chai';
import SearchForm from './SearchForm';
import React from 'react';
import {shallow} from 'enzyme';
import {spy} from 'sinon';


describe('SearchForm', () => {
    it('it renders a form', () => {
        const wrapper = shallow(<SearchForm/>);
        expect(wrapper.contains(
                <form></form>
            ))
            .to
            .equal(false);
    });
});
