import React, { Component, PropTypes } from 'react';
import MoviesList from '../MoviesList/MoviesList';
import SearchMovies from '../SearchForm/SearchForm';
import getData from '../../helpers/get';

export default class SearchApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            movies: []
        };
    }
    
     fetchMovies = async (query) => {
        const movies = await getData(query);
        try {
            const movies = await getData(query);
            this.setState({ movies: movies.results })
        } catch (err) {
            console.error(`Error: ${err.message}`);
        }
    }

    renderList () {
        const { movies } = this.state;    
        return (movies.length > 0) && <MoviesList movies={movies}/>;
    }

    render() {
        return (
            <div>
                <SearchMovies search={this.fetchMovies}/>
                {this.renderList()}
            </div>
        );
    }
}