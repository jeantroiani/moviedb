export default async function getData(query) {
    const baseUrl = `https://api.themoviedb.org/3/search/movie?api_key=3aef04b22988b95811069c2bf401c385&language=en-US&query=${query}&page=1&include_adult=false`;
    const response = await fetch(baseUrl);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
}