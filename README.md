#DAZN Test

## Install dependencies
npm install

## To run the application
npm run start
and open in your [browser](http://localhost:8080)

## To run test
npm run test